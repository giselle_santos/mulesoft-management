%dw 1.0
%output application/java
---
[{
	0: "{
  "name": "Gerdau",
  "id": "402c707c-9264-4c98-868f-2274bfcd0e08",
  "createdAt": "2017-03-16T16:57:39.825Z",
  "updatedAt": "2018-05-24T01:11:25.461Z",
  "ownerId": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
  "clientId": "12a5dc36904a40ce8dd0a32332f18318",
  "domain": "gerdau",
  "idprovider_id": "mulesoft",
  "isFederated": false,
  "parentOrganizationIds": [],
  "subOrganizationIds": [
    "c95fa37e-e9be-4e31-8953-6d09f0e4fac7",
    "6ea8d099-1d3c-4645-b89c-a199d002eefc",
    "862d98fa-7a2a-484e-9484-29f8e7aa064b",
    "ef6b0137-f8cd-4f74-8929-dc65d70aa3a9",
    "c00ac9fc-7234-4c04-af53-c3148c4ca0a1",
    "1d53cfc0-2bd2-438e-9d49-8b5a28e5f9fe",
    "cf9374c6-98ca-45f7-b2cf-c03088e23918",
    "2fdf19d4-71c7-45c8-8e71-3496f362714b",
    "99b73a7f-72aa-429b-8840-62d6b47daaad",
    "7a0b3ce9-c84a-4e53-8034-ad1b3914dce7",
    "d2a7b85c-0883-496d-abb3-26f00fa6aff3",
    "bdc20ac7-ea4b-4677-a22e-26964dd1f3ac"
  ],
  "tenantOrganizationIds": [],
  "isMaster": true,
  "subscription": {
    "category": "Customer",
    "type": "Platinum",
    "expiration": "2019-03-14T16:57:39.820Z"
  },
  "properties": {
    "cs_auth": {
      "session_timeout": 10800000
    },
    "exchangeCustomization": {
      "projectId": "0be307c4-5105-4381-bc10-ca2daadbd700",
      "publishedVersionReferenceId": "ac98335b005ff1c84ef7c63bc9ba770da99c4de4",
      "assetId": {
        "groupId": "402c707c-9264-4c98-868f-2274bfcd0e08",
        "assetId": "62384555-a707-4d8a-916d-e762c3d6687a",
        "version": "1.12.0"
      },
      "projectMigrationStatus": "branching-model"
    }
  },
  "environments": [
    {
      "id": "7fc337e2-4f3d-4aca-82c8-d4203e7d943d",
      "name": "Development",
      "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "3d71e658bc6c48d6bebdd59e78eac4f1"
    },
    {
      "id": "19ee468b-2f76-47a3-9f61-5970cee1793b",
      "name": "Production",
      "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
      "isProduction": true,
      "type": "production",
      "clientId": "d4baf17a99904571ae5b9fff6db1c52d"
    },
    {
      "id": "73ac2fa2-9bdf-4cb4-a434-47135df5b2fc",
      "name": "Quality",
      "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "a9c61827c65f462b984e7f8507594cbb"
    }
  ],
  "entitlements": {
    "createEnvironments": true,
    "globalDeployment": true,
    "createSubOrgs": true,
    "hybrid": {
      "enabled": true
    },
    "hybridInsight": true,
    "hybridAutoDiscoverProperties": true,
    "vCoresProduction": {
      "assigned": 10,
      "reassigned": 6.2
    },
    "vCoresSandbox": {
      "assigned": 20,
      "reassigned": 11.200000000000001
    },
    "vCoresDesign": {
      "assigned": 20,
      "reassigned": 8.9
    },
    "staticIps": {
      "assigned": 20,
      "reassigned": 3
    },
    "vpcs": {
      "assigned": 2,
      "reassigned": 0
    },
    "vpns": {
      "assigned": 0,
      "reassigned": 0
    },
    "workerLoggingOverride": {
      "enabled": false
    },
    "mqMessages": {
      "base": 0,
      "addOn": 0
    },
    "mqRequests": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreRequestUnits": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreKeys": {
      "base": 0,
      "addOn": 0
    },
    "mqAdvancedFeatures": {
      "enabled": false
    },
    "gateways": {
      "assigned": 2147483647
    },
    "designCenter": {
      "api": true,
      "mozart": true,
      "apiVisual": true
    },
    "partnersProduction": {
      "assigned": 0
    },
    "partnersSandbox": {
      "assigned": 0
    },
    "loadBalancer": {
      "assigned": 2,
      "reassigned": 0
    },
    "externalIdentity": true,
    "autoscaling": false,
    "armAlerts": true,
    "apis": {
      "enabled": true
    },
    "apiMonitoring": {
      "schedules": 5
    },
    "monitoringCenter": {
      "productSKU": 0
    },
    "crowd": {
      "hideApiManagerDesigner": true,
      "enableApiDesigner": true,
      "environments": true
    },
    "cam": {
      "enabled": false
    },
    "exchange2": {
      "enabled": false
    },
    "crowdSelfServiceMigration": {
      "enabled": false
    },
    "kpiDashboard": {
      "enabled": false
    },
    "pcf": false,
    "appViz": false,
    "runtimeFabric": false,
    "anypointSecurityTokenization": {
      "enabled": false
    },
    "anypointSecurityEdgePolicies": {
      "enabled": false
    },
    "messaging": {
      "assigned": 0
    },
    "workerClouds": {
      "assigned": 0,
      "reassigned": 0
    }
  },
  "owner": {
    "id": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
    "createdAt": "2017-03-16T16:57:39.807Z",
    "updatedAt": "2018-09-05T17:31:08.099Z",
    "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
    "firstName": "Bruno",
    "lastName": "Souza",
    "email": "Bruno.souza5@gerdau.com.br",
    "phoneNumber": "5511994328668",
    "idprovider_id": "mulesoft",
    "username": "Brunosouza",
    "enabled": true,
    "deleted": false,
    "lastLogin": "2018-09-05T17:31:00.000Z",
    "type": "host"
  },
  "sessionTimeout": 180
}",
1: "{
  "name": "HR",
  "id": "ef6b0137-f8cd-4f74-8929-dc65d70aa3a9",
  "createdAt": "2017-08-17T17:10:29.342Z",
  "updatedAt": "2018-06-15T13:02:36.044Z",
  "ownerId": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
  "clientId": "ca02b7e844e142f7a3a46bda40a04d40",
  "domain": null,
  "idprovider_id": "mulesoft",
  "isFederated": false,
  "parentOrganizationIds": [
    "402c707c-9264-4c98-868f-2274bfcd0e08"
  ],
  "subOrganizationIds": [],
  "tenantOrganizationIds": [],
  "isMaster": false,
  "properties": {},
  "environments": [
    {
      "id": "97aa64db-0af3-4e5e-ad5b-3417c22a6ef6",
      "name": "Development",
      "organizationId": "ef6b0137-f8cd-4f74-8929-dc65d70aa3a9",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "778740adba224559ade8727628bd021d"
    },
    {
      "id": "a5f75d8f-f1da-4fbd-acc9-f1e63c7e189f",
      "name": "Production",
      "organizationId": "ef6b0137-f8cd-4f74-8929-dc65d70aa3a9",
      "isProduction": true,
      "type": "production",
      "clientId": "526199de3d7846a1ba3a69e0931d8dcd"
    },
    {
      "id": "b66b746d-b649-42ac-8cf8-943088783286",
      "name": "Quality",
      "organizationId": "ef6b0137-f8cd-4f74-8929-dc65d70aa3a9",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "9c49a09507db42e8b877b8831c42ab6c"
    }
  ],
  "entitlements": {
    "createEnvironments": true,
    "globalDeployment": true,
    "createSubOrgs": true,
    "hybrid": {
      "enabled": true
    },
    "hybridInsight": true,
    "hybridAutoDiscoverProperties": true,
    "vCoresProduction": {
      "assigned": 0.4,
      "reassigned": 0
    },
    "vCoresSandbox": {
      "assigned": 0.8,
      "reassigned": 0
    },
    "vCoresDesign": {
      "assigned": 0.8,
      "reassigned": 0
    },
    "staticIps": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpcs": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpns": {
      "assigned": 0,
      "reassigned": 0
    },
    "workerLoggingOverride": {
      "enabled": false
    },
    "mqMessages": {
      "base": 0,
      "addOn": 0
    },
    "mqRequests": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreRequestUnits": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreKeys": {
      "base": 0,
      "addOn": 0
    },
    "mqAdvancedFeatures": {
      "enabled": false
    },
    "designCenter": {
      "api": true,
      "mozart": true,
      "apiVisual": true
    },
    "partnersProduction": {
      "assigned": 0
    },
    "partnersSandbox": {
      "assigned": 0
    },
    "loadBalancer": {
      "assigned": 0,
      "reassigned": 0
    },
    "apiMonitoring": {
      "schedules": 5
    },
    "monitoringCenter": {
      "productSKU": 0
    },
    "kpiDashboard": {
      "enabled": false
    },
    "pcf": false,
    "appViz": false,
    "runtimeFabric": false,
    "anypointSecurityTokenization": {
      "enabled": false
    },
    "anypointSecurityEdgePolicies": {
      "enabled": false
    },
    "messaging": {
      "assigned": 0
    },
    "workerClouds": {
      "assigned": 0,
      "reassigned": 0
    }
  },
  "owner": {
    "id": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
    "createdAt": "2017-03-16T16:57:39.807Z",
    "updatedAt": "2018-09-05T17:31:08.099Z",
    "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
    "firstName": "Bruno",
    "lastName": "Souza",
    "email": "Bruno.souza5@gerdau.com.br",
    "phoneNumber": "5511994328668",
    "idprovider_id": "mulesoft",
    "username": "Brunosouza",
    "enabled": true,
    "deleted": false,
    "lastLogin": "2018-09-05T17:31:00.000Z",
    "type": "host"
  }
}",
3:"{
  "name": "Metallics",
  "id": "2fdf19d4-71c7-45c8-8e71-3496f362714b",
  "createdAt": "2017-12-28T16:04:15.422Z",
  "updatedAt": "2018-08-22T18:04:49.696Z",
  "ownerId": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
  "clientId": "a951aa02ad334d6bb80189b04f010331",
  "domain": null,
  "idprovider_id": "mulesoft",
  "isFederated": false,
  "parentOrganizationIds": [
    "402c707c-9264-4c98-868f-2274bfcd0e08"
  ],
  "subOrganizationIds": [],
  "tenantOrganizationIds": [],
  "isMaster": false,
  "properties": {},
  "environments": [
    {
      "id": "27b8f3f7-d152-446b-b0c1-f166fb053d74",
      "name": "Development",
      "organizationId": "2fdf19d4-71c7-45c8-8e71-3496f362714b",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "b65149359b1f439887b912fe43632d9d"
    },
    {
      "id": "da8c5de3-5a02-48c8-bcb5-bfeae3fba58a",
      "name": "Production",
      "organizationId": "2fdf19d4-71c7-45c8-8e71-3496f362714b",
      "isProduction": true,
      "type": "production",
      "clientId": "bb0b0ea512ec48ff8c230cf6c486a6f4"
    },
    {
      "id": "1d1fea2d-1fd0-4df9-af3a-89027a047d71",
      "name": "Quality",
      "organizationId": "2fdf19d4-71c7-45c8-8e71-3496f362714b",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "2c8ae72c4aea4750a0a450d216f9a931"
    }
  ],
  "entitlements": {
    "createEnvironments": true,
    "globalDeployment": true,
    "createSubOrgs": true,
    "hybrid": {
      "enabled": true
    },
    "hybridInsight": true,
    "hybridAutoDiscoverProperties": true,
    "vCoresProduction": {
      "assigned": 0.4,
      "reassigned": 0
    },
    "vCoresSandbox": {
      "assigned": 0.6,
      "reassigned": 0
    },
    "vCoresDesign": {
      "assigned": 0.4,
      "reassigned": 0
    },
    "staticIps": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpcs": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpns": {
      "assigned": 0,
      "reassigned": 0
    },
    "workerLoggingOverride": {
      "enabled": false
    },
    "mqMessages": {
      "base": 0,
      "addOn": 0
    },
    "mqRequests": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreRequestUnits": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreKeys": {
      "base": 0,
      "addOn": 0
    },
    "mqAdvancedFeatures": {
      "enabled": false
    },
    "designCenter": {
      "api": true,
      "mozart": true,
      "apiVisual": true
    },
    "partnersProduction": {
      "assigned": 0
    },
    "partnersSandbox": {
      "assigned": 0
    },
    "loadBalancer": {
      "assigned": 0,
      "reassigned": 0
    },
    "apiMonitoring": {
      "schedules": 5
    },
    "monitoringCenter": {
      "productSKU": 0
    },
    "kpiDashboard": {
      "enabled": false
    },
    "pcf": false,
    "appViz": false,
    "runtimeFabric": false,
    "anypointSecurityTokenization": {
      "enabled": false
    },
    "anypointSecurityEdgePolicies": {
      "enabled": false
    },
    "messaging": {
      "assigned": 0
    },
    "workerClouds": {
      "assigned": 0,
      "reassigned": 0
    }
  },
  "owner": {
    "id": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
    "createdAt": "2017-03-16T16:57:39.807Z",
    "updatedAt": "2018-09-05T17:31:08.099Z",
    "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
    "firstName": "Bruno",
    "lastName": "Souza",
    "email": "Bruno.souza5@gerdau.com.br",
    "phoneNumber": "5511994328668",
    "idprovider_id": "mulesoft",
    "username": "Brunosouza",
    "enabled": true,
    "deleted": false,
    "lastLogin": "2018-09-05T17:31:00.000Z",
    "type": "host"
  }
}"
} as :object {
	class : "java.util.HashSet"
}]