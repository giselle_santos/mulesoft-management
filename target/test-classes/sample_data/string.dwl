%dw 1.0
%output application/java
---
"[{"id":"4dae221b-8b27-405d-b05a-748ac059d5dd","name":"Development","organizationId":"c00ac9fc-7234-4c04-af53-c3148c4ca0a1","isProduction":false,"type":"sandbox","clientId":"422e9168e2274b4ebe4f085715337f3f"},
{"id":"73ac2fa2-9bdf-4cb4-a434-47135df5b2fc","name":"Quality","organizationId":"402c707c-9264-4c98-868f-2274bfcd0e08","isProduction":false,"type":"sandbox","clientId":"a9c61827c65f462b984e7f8507594cbb"},
{"id":"c14d5841-8c3e-40b7-8110-282fb7f9f2f7","name":"Production","organizationId":"99b73a7f-72aa-429b-8840-62d6b47daaad","isProduction":true,"type":"production","clientId":"dabd9e0f209a47889868104402b131c7"},
{"id":"f4bf15e2-3117-409d-ba55-946c930dde6d","name":"Production","organizationId":"7a0b3ce9-c84a-4e53-8034-ad1b3914dce7","isProduction":true,"type":"production","clientId":"7560eb6c335740c0917a2980c48cbb5e"},
{"id":"0c44f3e4-c099-4a31-bbe7-3cbdcab5d6c7","name":"Quality","organizationId":"d2a7b85c-0883-496d-abb3-26f00fa6aff3","isProduction":false,"type":"sandbox","clientId":"7e6f2cdd15174c61bda0a0a4e40076f6"},
{"id":"1f4af159-9b01-4f59-a999-4dbf34f000b9","name":"Development","organizationId":"1d53cfc0-2bd2-438e-9d49-8b5a28e5f9fe","isProduction":false,"type":"sandbox","clientId":"1a55179ec873435a99643afc976418ef"},
{"id":"6269ae92-7f2a-437b-ba8f-9091cacb9f3e","name":"Production","organizationId":"6ea8d099-1d3c-4645-b89c-a199d002eefc","isProduction":true,"type":"production","clientId":"55f369740a6b4ac286467a9b7e4c483b"},
{"id":"dc289042-e66f-40a0-9c7b-c3c8754c9f21","name":"Development","organizationId":"7a0b3ce9-c84a-4e53-8034-ad1b3914dce7","isProduction":false,"type":"sandbox","clientId":"5f45b6dc3c024d5e864f2cf89d8b05ce"}]"