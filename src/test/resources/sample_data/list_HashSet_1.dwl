%dw 1.0
%output application/java
---
[{
	empty: {
  "name": "Financial",
  "id": "c00ac9fc-7234-4c04-af53-c3148c4ca0a1",
  "createdAt": "2017-09-27T19:27:16.886Z",
  "updatedAt": "2018-06-15T13:01:45.770Z",
  "ownerId": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
  "clientId": "80ef326803ac4215bc78a35a447fcf67",
  "domain": null,
  "idprovider_id": "mulesoft",
  "isFederated": false,
  "parentOrganizationIds": [
    "402c707c-9264-4c98-868f-2274bfcd0e08"
  ],
  "subOrganizationIds": [],
  "tenantOrganizationIds": [],
  "isMaster": false,
  "properties": {},
  "environments": [
    {
      "id": "4dae221b-8b27-405d-b05a-748ac059d5dd",
      "name": "Development",
      "organizationId": "c00ac9fc-7234-4c04-af53-c3148c4ca0a1",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "422e9168e2274b4ebe4f085715337f3f"
    },
    {
      "id": "f59a3221-711f-4772-a2ce-e3864b3d681d",
      "name": "Production",
      "organizationId": "c00ac9fc-7234-4c04-af53-c3148c4ca0a1",
      "isProduction": true,
      "type": "production",
      "clientId": "f51ad2806cae4fc196314c5aeaef59b3"
    },
    {
      "id": "a49b7343-a781-485c-91ef-11a40ad29ae2",
      "name": "Quality",
      "organizationId": "c00ac9fc-7234-4c04-af53-c3148c4ca0a1",
      "isProduction": false,
      "type": "sandbox",
      "clientId": "633f779be3494c079bec885b776872ff"
    }
  ],
  "entitlements": {
    "createEnvironments": true,
    "globalDeployment": true,
    "createSubOrgs": true,
    "hybrid": {
      "enabled": true
    },
    "hybridInsight": true,
    "hybridAutoDiscoverProperties": true,
    "vCoresProduction": {
      "assigned": 0.5,
      "reassigned": 0
    },
    "vCoresSandbox": {
      "assigned": 1,
      "reassigned": 0
    },
    "vCoresDesign": {
      "assigned": 1,
      "reassigned": 0
    },
    "staticIps": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpcs": {
      "assigned": 0,
      "reassigned": 0
    },
    "vpns": {
      "assigned": 0,
      "reassigned": 0
    },
    "workerLoggingOverride": {
      "enabled": false
    },
    "mqMessages": {
      "base": 0,
      "addOn": 0
    },
    "mqRequests": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreRequestUnits": {
      "base": 0,
      "addOn": 0
    },
    "objectStoreKeys": {
      "base": 0,
      "addOn": 0
    },
    "mqAdvancedFeatures": {
      "enabled": false
    },
    "designCenter": {
      "api": true,
      "mozart": true,
      "apiVisual": true
    },
    "partnersProduction": {
      "assigned": 0
    },
    "partnersSandbox": {
      "assigned": 0
    },
    "loadBalancer": {
      "assigned": 0,
      "reassigned": 0
    },
    "apiMonitoring": {
      "schedules": 5
    },
    "monitoringCenter": {
      "productSKU": 0
    },
    "kpiDashboard": {
      "enabled": false
    },
    "pcf": false,
    "appViz": false,
    "runtimeFabric": false,
    "anypointSecurityTokenization": {
      "enabled": false
    },
    "anypointSecurityEdgePolicies": {
      "enabled": false
    },
    "messaging": {
      "assigned": 0
    },
    "workerClouds": {
      "assigned": 0,
      "reassigned": 0
    }
  },
  "owner": {
    "id": "830c4674-8f11-40c9-b4e3-22e9b5c119b6",
    "createdAt": "2017-03-16T16:57:39.807Z",
    "updatedAt": "2018-09-12T13:39:07.017Z",
    "organizationId": "402c707c-9264-4c98-868f-2274bfcd0e08",
    "firstName": "Bruno",
    "lastName": "Souza",
    "email": "Bruno.souza5@gerdau.com.br",
    "phoneNumber": "5511994328668",
    "idprovider_id": "mulesoft",
    "username": "Brunosouza",
    "enabled": true,
    "deleted": false,
    "lastLogin": "2018-09-12T13:39:00.000Z",
    "type": "host"
  }
}
} as :object {
	class : "java.util.HashSet"
}]